<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeHistory extends Model
{
    protected $table = 'employee_web_history';
    protected $fillable = ["ip_addr", "url"];

    /**
     * Return the urls for this employee
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function employee()
    {
        return $this->hasOne('App\Employee', 'ip_addr', 'ip_addr');
    }
}
