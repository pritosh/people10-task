<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
        return response()->json(Employee::all()->toArray());
    }

    public function store(Request $request)
    {
    }

    public function show( $ip )
    {
        $employee = Employee::with('history')->where('ip_addr', $ip)->firstOrFail();
        return response()->json( $employee );
    }

    public function destroy($ip)
    {
        $employee = Employee::findOrFail( $ip );
        if(! $employee->delete() )
        { return response()->json(['result'=>false,'error'=>'could not delete'],500); }

        return response()->json(['result'=>true]);
    }
}
