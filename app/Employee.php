<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $fillable = ["emp_id", "emp_name", "ip_addr"];

     /**
     * @param Builder $query
     * @param $ip
     * @return mixed
     */
    public function scopeByIP( $query, $ip )
    {
        return $query->where( 'ip_addr', $ip );
    }

    /**
     * Return the urls for this employee
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function history()
    {
        return $this->hasMany('App\EmployeeHistory', 'ip_addr', 'ip_addr');
    }

}
