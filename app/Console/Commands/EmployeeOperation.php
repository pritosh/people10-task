<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Employee;

class EmployeeOperation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee {--all : List all Employee} 
    {--create : create new employee} 
    {--byIP : Fetch Employee by IP}
    {--delete : Delete employee by IP}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Employee Operation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if( $this->option('all') )
        {
            $data = $this->allEmployee();
        }
        if( $this->option('create') )
        {
            $emp_name = $this->ask('Please enter Employee Name :');
            if(empty($emp_name))
            {
                print "Employee name required";
                return;
            }
            $emp_id = $this->ask('Please enter Employee Id :');
            if(empty($emp_id))
            {
                print "Employee Id required";
                return;
                
            }
            $ip = $this->ask('Please enter IP :');
            if(empty($ip))
            {
                print "IP required";
                return;
                
            }
            $data = $this->createEmployee($emp_name, $emp_id, $ip);
        }
        if( $this->option('byIP') )
        {
            $ip = $this->ask('What is your IP?');
            if(empty($ip)) 
            { 
                print "IP required";
                return; 
            }
            $data = $this->employeByIp( $ip );
        }
        if( $this->option('delete') )
        {
            $ip = $this->ask('What is your IP?');
            if(empty($ip)) 
            { 
                print "IP required";
                return; 
            }
            $data = $this->deleteEmployee( $ip );
        }
        echo json_encode($data);
        
    }

    public function allEmployee()
    {
        return Employee::all()->toArray();
    }

    public function createEmployee($emp_name, $emp_id, $ip)
    {
        return Employee::create(["emp_name"=> $emp_name, "emp_id"=> $emp_id, "ip_addr" => $ip]);

    }

    public function employeByIp( $ip )
    {
        $employee = Employee::with('history')->where('ip_addr', $ip)->first();
        if (empty($employee)) 
        { return ['record not found'];}
        return $employee;
    }

    public function deleteEmployee( $ip )
    {
        $employee = Employee::where('ip_addr', $ip)->first();
        if (empty($employee)) 
        { return ['record not found'];}
        if(! $employee->delete() )
        { return response()->json(['result'=>false,'error'=>'could not delete'],500); }

        return ['Successfully Deleted'];

    }
}
