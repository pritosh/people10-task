<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EmployeeHistory;
use App\Employee;

class EmployeeHistoryOperation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employeehistory
    {--create : create new employee history} 
    {--byIP : Fetch Employee by IP}
    {--delete : Delete employee by IP}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'EmployeeHistory Operation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if( $this->option('create') )
        {
            $ip = $this->ask('Please enter IP :');
            if(empty($ip))
            {
                print "IP required";
                return;
                
            }
            $url = $this->ask('Please enter URL :');
            if(empty($url))
            {
                print "URL is required";
                return;
                
            }
            $data = $this->createEmployee( $ip, $url);
        }
        if( $this->option('byIP') )
        {
            $ip = $this->ask('What is your IP?');
            if(empty($ip)) 
            { 
                print "IP required";
                return; 
            }
            $data = $this->employeHistoryByIp( $ip );
        }
        if( $this->option('delete') )
        {
            $ip = $this->ask('What is your IP?');
            if(empty($ip)) 
            { 
                print "IP required";
                return; 
            }
            $data = $this->deleteEmployeeHistory( $ip );
        }
        echo json_encode($data);
        
    }

    public function createEmployee( $ip, $url)
    {
        $employee = Employee::ByIP($ip)->get();
        if($employee->isEmpty())
        {return ["IP is not associate with Employee"] ;}
        return EmployeeHistory::create(["ip_addr" => $ip, "url" => $url]);

    }

    public function employeHistoryByIp( $ip )
    {
        $employee = EmployeeHistory::with('employee')->where('ip_addr', $ip)->get();
        if ($employee->isEmpty()) 
        { return ['record not found'];}
        return $employee;
    }

    public function deleteEmployeeHistory( $ip )
    {
        $employee = EmployeeHistory::where('ip_addr', $ip)->get();
        if ($employee->isEmpty()) 
        { return ['record not found'];}
        foreach($employee as $emp)
        {
            EmployeeHistory::findorfail($emp->id)->delete();
        }

        return ['Successfully Deleted'];

    }
}
